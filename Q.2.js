//NAME:Maheshwar
//STUDENT ID: 27043591
//DATE:11/6/2018
//Definition of the "Employee" class
class Employee {

  //"default" getter and setter for the "_firstName" property
    get FirstName() {
        return this._firstName      //When the "FirstName" getter "function" is used, it returns the value of the "_firstName" property
    }


    set FirstName(value) {             //The "value" parameter has the value passed to it from the constructor                      
        this._firstName = value       //Creates the "_firstName" property and sets the value passed to it from the constructor                     
    }

 //"default" getter and setter for the "_lastName" property
    get LastName() {
        return this._lastName                    //When the "LastName" getter "function" is used, it returns the value of the "_lastName" property           
    }

    set LastName(value) {                          //The "value" parameter has the value passed to it from the constructor          
        this._lastName = value                     //Creates the "_lastName" property and sets the value passed to it from the constructor                                         
    }

    //"default" getter and setter for the "_salary" property
    get Salary(){
        return this._salary                         //When the "Salary" getter "function" is used, it returns the value of the "_salary" property 
    }

    set Salary(value) {                            //The "value" parameter has the value passed to it from the constructor
        this._salary = value                        //Creates the "_salary" property and sets the value passed to it from the constructor
    }

    //"default" getter and setter for the "_taxrate" property
    get TaxRate() {
        return this._taxrate                         //When the "TaxRate" getter "function" is used, it returns the value of the "_taxrate" property
    }

    set TaxRate(value) {                            //The "value" parameter has the value passed to it from the constructor
        this._taxrate = value                       //Creates the "_taxrate" property and sets the value passed to it from the constructor
    }

//Constructor for the "Employee" class
constructor(firstName, lastName, salary, taxrate) {
    this.FirstName = firstName                             //Pass the value of the "firstName" parameter to the "FirstName" setter
    this.LastName = lastName                              //Pass the value of the "lastName" parameter to the "LastName"  setter                            
    this.Salary = salary                                 //Pass the value of the "Salary" parameter to the "salary"  setter 
    this.TaxRate = taxrate                              //Pass the value of the "TaxRate" parameter to the "taxrate"  setter 
}

 //Method definition - "EmployeeInfo()"
    //Returns a string comprising of the Employee full details - first name, last name ,salary and taxrate
EmployeeInfo(){
    return `Employee Information: ${this._firstName} ${this._lastName}; $${this._salary}; ${this._taxrate}%`
}

//Method definition - "CalcAvg()"
    //Returns a number that is the average of the three numbers
CalcAvg() {
    return (this._salary -(this._salary * this._taxrate/ 100))
}
}
alert("Welcome");

//------------
//MAIN PROGRAM
//------------

console.log(`\n`)

//Instantiate a "blank" new object "EmployeeA" using the "Employee" class as a "template/blueprint"
let EmployeeA = new Employee()

//Ask user for "EmployeeA" property values (display to browser console as they are entered)
EmployeeA.FirstName = prompt("Enter Employee Detail. Enter the first name:")                 //Uses the "FirstName" setter
console.log(`First name entered : ${EmployeeA.FirstName}`)

EmployeeA.LastName = prompt("Enter Employee Detail. Enter the Last name:")                     //Uses the "LastName" setter
console.log(`Last name entered : ${EmployeeA.LastName}`)

EmployeeA.Salary = prompt("Enter Employee Detail. Enter Annual Gross Salary:")                    //Uses the "Salary" setter
console.log(`Annual Gross Salary entered : $${EmployeeA.Salary}`)

EmployeeA.TaxRate = prompt("Enter Employee Detail. Enter Tax Rate:")                              //Uses the "TaxRate" setter
console.log(`Tax Rate entered : ${EmployeeA.TaxRate}%`)

console.log(`\n\n\n`)
// Use the "EmployeeInfo()" Method to display the information of "EmployeeA"
console.log("Display the information of `EmployeeA`")
console.log("--------------------------------------")           //The "EmployeeInfo()"  Method is being used
console.log(`${EmployeeA.EmployeeInfo()}`)

console.log(`\n\n\n`)
//Use the "CalcAvg()" Method to display the required average of the numbers
console.log("Display the information of `EmployeeA` Net Salary")
console.log("-------------------------------------------------")
console.log(`Net Salary: $${EmployeeA.CalcAvg()}`)

